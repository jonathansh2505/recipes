package gtl.test.recipes.repository

import gtl.test.recipes.dao.RecipeDao
import gtl.test.recipes.model.Recipe
import gtl.test.recipes.model.RecipeResults
import gtl.test.recipes.service.RecipeService
import io.reactivex.Observable
import javax.inject.Inject

class RecipeRepository @Inject constructor(
    private val recipeService: RecipeService,
    private val recipeDao: RecipeDao
) {
    fun getRecipes(search : String) : Observable<RecipeResults> {
        return recipeService.getRecipes(search)
    }

    fun getRecipeDataFromNetwork(recipeId : Long): Observable<Recipe> {
        return recipeService.getRecipeData(recipeId)
    }

    fun getRecipeDataFromDatabase(recipeId : Long): Recipe {
        return recipeDao.getRecipe(recipeId)
    }

    suspend fun saveRecipeToDatabase(recipe : Recipe) {
        return recipeDao.insertRecipe(recipe)
    }
}