package gtl.test.recipes.viewmodel

import android.content.Context
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import gtl.test.recipes.model.RecipeResults
import gtl.test.recipes.repository.RecipeRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject




@HiltViewModel
class SearchRecipesViewModel @Inject constructor(
    private val recipeRepository : RecipeRepository
) : ViewModel() {

    var compositeDisposable = CompositeDisposable()

    val showProgress : MutableLiveData<Boolean> = MutableLiveData(false)
    val showTypeRecipe : MutableLiveData<Boolean> = MutableLiveData(true)
    val showError : MutableLiveData<Boolean> = MutableLiveData(false)
    val showNoResults : MutableLiveData<Boolean> = MutableLiveData(false)
    val recipesResult : MutableLiveData<RecipeResults> = MutableLiveData()

    val searchActionListener: TextView.OnEditorActionListener

    init {
        this.searchActionListener = TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard(v)
                getRecipes(v.text.toString().trim())
                true
            } else false
        }
    }

    //This function should be in another place
    private fun hideKeyboard(v: View){
        val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }

    private fun getRecipes(search : String){
        this.showTypeRecipe.postValue(false)
        this.showProgress.postValue(true)
        this.showError.postValue(false)
        this.showNoResults.postValue(false)
        val disposable = recipeRepository.getRecipes(search)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ results ->
                this.showProgress.postValue(false)
                this.showNoResults.postValue(results.results.isEmpty())
                this.recipesResult.postValue(results)
            }) {
                this.showProgress.postValue(false)
                this.showError.postValue(true)
                print(it.message)
            }
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}