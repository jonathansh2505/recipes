package gtl.test.recipes.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import gtl.test.recipes.model.Recipe
import gtl.test.recipes.repository.RecipeRepository
import gtl.test.recipes.util.DateUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject




@HiltViewModel
class RecipeDetailsViewModel @Inject constructor(
    private val recipeRepository : RecipeRepository
) : ViewModel() {
    private val job = SupervisorJob()
    val coroutineScope = CoroutineScope(Dispatchers.IO + job)

    var compositeDisposable = CompositeDisposable()

    val recipe : MutableLiveData<Recipe?> = MutableLiveData()

    val showProgress : MutableLiveData<Boolean> = MutableLiveData(false)
    val showError : MutableLiveData<Boolean> = MutableLiveData(false)

    fun getRecipeDetailsFromDatabase(recipeId: Long){
        this.showProgress.postValue(true)
        this.showError.postValue(false)
        coroutineScope.launch {
            val savedRecipe = recipeRepository.getRecipeDataFromDatabase(recipeId);
            if (savedRecipe != null && DateUtil.isValidRecipeDate(savedRecipe.timestamp)) {
                showProgress.postValue(false)
                recipe.postValue(savedRecipe)
            } else {
                getRecipeDetailsFromNetwork(recipeId)
            }
        }
    }

    private fun saveRecipe(recipe : Recipe){
        coroutineScope.launch {
            recipeRepository.saveRecipeToDatabase(recipe)
        }
    }

    fun getRecipeDetailsFromNetwork(recipeId : Long){
        this.showProgress.postValue(true)
        this.showError.postValue(false)
        val disposable = recipeRepository.getRecipeDataFromNetwork(recipeId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ resultRecipe ->
                this.showProgress.postValue(false)
                this.recipe.postValue(resultRecipe)
                resultRecipe.timestamp = System.currentTimeMillis()
                saveRecipe(resultRecipe)
            }) {
                this.showProgress.postValue(false)
                this.showError.postValue(true)
                print(it.message)
            }
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        job.cancel()
    }
}