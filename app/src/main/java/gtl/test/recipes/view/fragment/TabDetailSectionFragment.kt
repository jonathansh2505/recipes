package gtl.test.recipes.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import gtl.test.recipes.R
import gtl.test.recipes.util.TextUtil

class TabDetailSectionFragment : Fragment() {
    companion object {
        const val TEXT_TO_SHOW = "textToShow"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_tab_detail_section, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(TEXT_TO_SHOW) }?.apply {
            val textView: TextView = view.findViewById(R.id.textview_description)
            getString(TEXT_TO_SHOW)?.let {
                textView.text = TextUtil.getHtmlText(it)
            }
        }
    }
}