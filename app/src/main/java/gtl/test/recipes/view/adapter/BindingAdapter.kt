package gtl.test.recipes.view.adapter

import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import gtl.test.recipes.R
import gtl.test.recipes.constant.Constants
import gtl.test.recipes.model.Recipe


@BindingAdapter("setRecipesAdapter")
fun setRecipesAdapter(
    recyclerView: RecyclerView,
    adapter: RecipesAdapter?
) {
    adapter?.let {
        recyclerView.adapter = it
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("setRecipeList")
fun setRecipeList(
    recyclerView: RecyclerView,
    recipes: List<Recipe>?
) {
    val adapter = recyclerView.adapter as BaseAdapter<ViewDataBinding, ListAdapterItem>?
    adapter?.setDataList(recipes ?: listOf())
}

@BindingAdapter("setImageListItem")
fun setImageListItem(imageView: ImageView, imageUrl: String?) {
    Glide.with(imageView.context)
        .load(Constants.BASE_IMAGE_URL + imageUrl) //The base image URL should be taken from response field called "baseUri" for a good implementation
        .centerCrop()
        .into(imageView)
}

@BindingAdapter("setImageDetailItem")
fun setImageDetailItem(imageView: ImageView, imageUrl: String?) {
    Glide.with(imageView.context)
        .load(imageUrl)
        .centerCrop()
        .into(imageView)
}

@BindingAdapter("setReadyMinutesText")
fun setReadyMinutesText(textView: TextView, minutes: Int) {
    textView.text = textView.context.getString(R.string.ready_in_minutes, minutes)
}

@BindingAdapter("setEditorActionListener")
fun setEditorActionListener(editText: AppCompatEditText, editorActionListener: TextView.OnEditorActionListener) {
    editText.setOnEditorActionListener(editorActionListener)
}