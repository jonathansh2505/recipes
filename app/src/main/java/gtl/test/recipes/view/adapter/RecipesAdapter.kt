package gtl.test.recipes.view.adapter

import gtl.test.recipes.R
import gtl.test.recipes.databinding.RecipeListItemBinding
import gtl.test.recipes.listener.RecipeClickListener
import gtl.test.recipes.model.Recipe

class RecipesAdapter(
    recipeList: List<Recipe>,
    private val recipeClickListener: RecipeClickListener
) : BaseAdapter<RecipeListItemBinding, Recipe>(recipeList) {

    override val layoutId: Int = R.layout.recipe_list_item

    override fun bind(binding: RecipeListItemBinding, item: Recipe) {
        binding.apply {
            this.item = item
            this.listener = recipeClickListener
            executePendingBindings()
        }
    }
}