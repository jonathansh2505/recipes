package gtl.test.recipes.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import gtl.test.recipes.R
import gtl.test.recipes.databinding.FragmentRecipeDetailsBinding
import gtl.test.recipes.model.Recipe
import gtl.test.recipes.util.TextUtil
import gtl.test.recipes.viewmodel.RecipeDetailsViewModel

@AndroidEntryPoint
class RecipeDetailsFragment : Fragment(), View.OnClickListener {

    private val args: RecipeDetailsFragmentArgs by navArgs()
    private lateinit var binding: FragmentRecipeDetailsBinding

    private lateinit var viewModel: RecipeDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(RecipeDetailsViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recipe_details, container, false)
        binding.apply {
            viewModel = this@RecipeDetailsFragment.viewModel
            lifecycleOwner = this@RecipeDetailsFragment
        }
        val view = binding.root
        binding.viewModel = viewModel
        binding.buttonClose.setOnClickListener(this)
        viewModel.recipe.observe(viewLifecycleOwner, { recipe ->
            recipe?.let {
                binding.viewPager.adapter = TabDetailSectionAdapter(this, recipe)
                TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                    tab.text = getString(
                        when (position) {
                            0 -> R.string.summary
                            1 -> R.string.ingredients
                            else -> R.string.instructions
                        }
                    )
                }.attach()
            }
        })
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recipeId = args.recipeId
        viewModel.getRecipeDetailsFromDatabase(recipeId)
    }

    override fun onClick(view: View) {
        when(view.id){
            R.id.button_close -> {
                activity?.onBackPressed()
            }
        }
    }

    class TabDetailSectionAdapter(fragment: Fragment, val recipe : Recipe) : FragmentStateAdapter(fragment) {

        override fun getItemCount(): Int = 3

        override fun createFragment(position: Int): Fragment {

            // Return a NEW fragment instance in createFragment(int)
            val fragment = TabDetailSectionFragment()
            fragment.arguments = Bundle().apply {
                putString(
                    TabDetailSectionFragment.TEXT_TO_SHOW, when (position) {
                        0 -> recipe.summary
                        1 -> TextUtil.getIngredientsText(recipe.extendedIngredients)
                        else -> recipe.instructions
                    }
                )
            }
            return fragment
        }
    }
}