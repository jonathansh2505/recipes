package gtl.test.recipes.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import gtl.test.recipes.R
import gtl.test.recipes.databinding.FragmentSearchRecipeBinding
import gtl.test.recipes.listener.RecipeClickListener
import gtl.test.recipes.model.Recipe
import gtl.test.recipes.view.adapter.RecipesAdapter
import gtl.test.recipes.viewmodel.SearchRecipesViewModel

@AndroidEntryPoint
class SearchRecipeFragment : Fragment(), RecipeClickListener {

    private lateinit var binding: FragmentSearchRecipeBinding

    private lateinit var viewModel: SearchRecipesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SearchRecipesViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_recipe, container, false)
        binding.apply {
            viewModel = this@SearchRecipeFragment.viewModel
            lifecycleOwner = this@SearchRecipeFragment
        }
        val view = binding.root
        binding.viewModel = viewModel
        binding.adapter = RecipesAdapter(listOf(), this)
        return view
    }

    override fun onClickListener(recipe: Recipe) {
        val action = SearchRecipeFragmentDirections.actionSearchRecipeFragmentToRecipeDetailsFragment(recipe.id)
        view?.findNavController()?.navigate(action)
    }
}