package gtl.test.recipes.util

import android.os.Build
import android.text.Html
import android.text.Spanned
import gtl.test.recipes.model.Ingredient

class TextUtil {
    companion object{
        fun getHtmlText(htmlText : String): Spanned? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(htmlText, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(htmlText)
            }
        }

        fun getIngredientsText(ingredients : List<Ingredient>?): String {
            ingredients?.let{
                var resultText = ""
                for (ingredient in ingredients){
                    resultText = resultText.plus("${ingredient.originalString}\n")
                }
                return resultText
            }
            return ""
        }
    }
}