package gtl.test.recipes.util

import java.util.*
import java.util.concurrent.TimeUnit

class DateUtil {
    companion object{
        fun isValidRecipeDate(timestamp : Long) : Boolean{
            val today = Calendar.getInstance()
            return TimeUnit.DAYS.convert(today.timeInMillis - timestamp, TimeUnit.MILLISECONDS) <= 7
        }
    }
}