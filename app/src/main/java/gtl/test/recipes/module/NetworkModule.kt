package gtl.test.recipes.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import gtl.test.recipes.constant.Constants
import gtl.test.recipes.qualifier.ApiKeyInterceptorHttpClient
import gtl.test.recipes.service.RecipeService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun providesApiKeyInterceptor(): Interceptor = ApiKeyInterceptor()

    @ApiKeyInterceptorHttpClient
    @Provides
    fun providesAuthOkHttpClient(
        apiKeyInterceptor: Interceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(apiKeyInterceptor)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    @Provides
    fun provideNetworkService(
        @ApiKeyInterceptorHttpClient apiKeyHttpClient : OkHttpClient
    ): RecipeService {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(apiKeyHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(RecipeService::class.java)
    }
}