package gtl.test.recipes.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import gtl.test.recipes.constant.Constants
import gtl.test.recipes.dao.IngredientsConverter
import gtl.test.recipes.dao.RecipeDao
import gtl.test.recipes.dao.RecipeDatabase
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    fun provideChannelDao(recipeDatabase: RecipeDatabase): RecipeDao {
        return recipeDatabase.recipeDao()
    }

    @Provides
    @Singleton
    fun provideRecipeDatabase(@ApplicationContext appContext: Context): RecipeDatabase {
        return Room.databaseBuilder(
            appContext,
            RecipeDatabase::class.java,
            Constants.DATABASE_NAME
        )
        .addTypeConverter(IngredientsConverter())
        .build()
    }
}