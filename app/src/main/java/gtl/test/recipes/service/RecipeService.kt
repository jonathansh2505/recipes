package gtl.test.recipes.service

import gtl.test.recipes.constant.Constants
import gtl.test.recipes.model.Recipe
import gtl.test.recipes.model.RecipeResults
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RecipeService {
    @GET(Constants.SEARCH_RECIPES_URL)
    fun getRecipes(@Query("query") search : String): Observable<RecipeResults>

    @GET(Constants.RECIPE_DATA_URL)
    fun getRecipeData(@Path("recipeId") recipeId : Long): Observable<Recipe>
}