package gtl.test.recipes.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import gtl.test.recipes.view.adapter.ListAdapterItem

@Entity
data class Recipe(
    @PrimaryKey override val id: Long,
    val title: String,
    val readyInMinutes: Int,
    val image: String,
    val summary: String,
    val instructions : String,
    val extendedIngredients : List<Ingredient>,
    var timestamp : Long
    /*val servings: Int,
    val sourceUrl: String,
    val openLicense: Int,
    val imageType: String,
    val vegetarian : Boolean,
    val vegan : Boolean,
    val glutenFree : Boolean,
    val dairyFree : Boolean,
    val veryHealthy : Boolean,
    val cheap : Boolean,
    val veryPopular : Boolean,
    val sustainable : Boolean,
    val weightWatcherSmartPoints : Int,
    val gaps : String,
    val lowFodmap : Boolean,
    val aggregateLikes : Int,
    val spoonacularScore : Float,
    val healthScore : Float,
    val creditsText : String,
    val license : String,
    val sourceName : String,
    val pricePerServing : Float,
    val originalId : Long?,
    val spoonacularSourceUrl : String,*/
) : ListAdapterItem