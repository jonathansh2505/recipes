package gtl.test.recipes.model

data class Ingredient(
     val id : Long,
     val originalString : String
)
