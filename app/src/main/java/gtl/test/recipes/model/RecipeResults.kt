package gtl.test.recipes.model

data class RecipeResults(
    val results : List<Recipe>,
    val baseUri : String,
    val offset : Int,
    val number : Int,
    val totalResults : Int,
    val processingTimeMs : Int,
    val expires : Long,
    val isStale : Boolean
    )
