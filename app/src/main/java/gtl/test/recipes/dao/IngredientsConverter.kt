package gtl.test.recipes.dao

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import gtl.test.recipes.model.Ingredient
import java.lang.reflect.Type
import java.util.*

@ProvidedTypeConverter
class IngredientsConverter {

    @TypeConverter
    fun ingredientsToString(ingredients : List<Ingredient>) : String?{
        return Gson().toJson(ingredients)
    }

    @TypeConverter
    fun stringToIngredients(string : String?) : List<Ingredient>?{
        if (string == null) {
            return Collections.emptyList()
        }
        val listType: Type = object :
            TypeToken<List<Ingredient?>?>() {}.type
        return Gson().fromJson<List<Ingredient>>(string, listType)
    }

}