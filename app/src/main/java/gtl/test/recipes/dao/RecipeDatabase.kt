package gtl.test.recipes.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import gtl.test.recipes.model.Recipe

@Database(entities = [Recipe::class], version = 1)
@TypeConverters(IngredientsConverter::class)
abstract class RecipeDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao
}