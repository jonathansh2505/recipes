package gtl.test.recipes.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import gtl.test.recipes.model.Recipe

@Dao
interface RecipeDao {
    @Query("SELECT * FROM recipe WHERE id = :recipeId")
    fun getRecipe(recipeId : Long) : Recipe

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecipe(recipe: Recipe)
}