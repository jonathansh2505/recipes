package gtl.test.recipes.constant

class Constants {
    companion object {
        const val DATABASE_NAME = "recipes"
        const val BASE_URL = "https://api.spoonacular.com/recipes/"
        const val BASE_IMAGE_URL = "https://spoonacular.com/recipeImages/"
        const val SEARCH_RECIPES_URL = "search"
        const val RECIPE_DATA_URL = "{recipeId}/information?"
    }
}