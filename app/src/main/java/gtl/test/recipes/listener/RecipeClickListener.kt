package gtl.test.recipes.listener

import gtl.test.recipes.model.Recipe

interface RecipeClickListener {
    fun onClickListener(recipe: Recipe)
}